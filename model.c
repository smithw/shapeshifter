#include "model.h"


void shape_positiondump(unsigned short * pblocks, size_t pblock_count, Dimension dim, char * repr);
void shapeset_ordereddata(Shape * first, unsigned short change_order);
Shape * shapeset__breaksort(Shape *, unsigned short int (*)(Shape *, Shape *));
Shape * shapeset__mergesort(Shape *, Shape *, unsigned short int (*)(Shape *, Shape *));

// Shape functions

Shape * shape_init(char * desc, Dimension grid_dim) {
	char * dcp, * tk;
	unsigned int tksz, maxblocks, maxX, maxY, current_pos_idx, current_block;
	Shape * s;
	Position p, block_pos;
	
	// Shape data
	
	maxblocks      = 8;
	s              = malloc(sizeof(Shape));
	s->raw_blocks  = malloc(sizeof(Position) * maxblocks);
	s->block_count = 0;
	s->dim.w       = 0;
	s->dim.h       = 0;
	
	dcp = strdup(desc);
	
	for (p.y = 0, tk = strtok(dcp, ","); tk; tk = strtok(NULL, ","), p.y++) {
		tksz = strlen(tk);
		
		for (p.x = 0; p.x < tksz; p.x++) {
			if ((tk[p.x] == '0') || (tk[p.x] == '.')) continue;
			
			if (s->block_count >= maxblocks) {
				maxblocks     *= 2;
				s->raw_blocks  = realloc(s->raw_blocks, sizeof(Position) * maxblocks);
			}
			
			s->raw_blocks[s->block_count] = p;
			
			s->block_count++;
		}
		
		if (p.x > s->dim.w) s->dim.w = p.x;
	}
	
	free(dcp);
	
	s->dim.h = p.y;
	
	if (maxblocks != s->block_count) s->raw_blocks = realloc(s->raw_blocks, sizeof(Position) * s->block_count);
	
	// Grid-dependent data
	
	maxY = grid_dim.h - s->dim.h;
	maxX = grid_dim.w - s->dim.w;
	
	s->position_count    = (maxX + 1) * (maxY + 1);
	s->positions         = malloc(sizeof(Position) * s->position_count);
	s->positioned_blocks = malloc(sizeof(unsigned short *) * s->position_count);
	
	for (p.y = 0, current_pos_idx = 0; p.y <= maxY; p.y++) {
		for (p.x = 0; p.x <= maxX; p.x++, current_pos_idx++) {
			s->positioned_blocks[current_pos_idx] = malloc(sizeof(unsigned short) * s->block_count);
			s->positions[current_pos_idx]         = p;
			
			for (current_block = 0; current_block < s->block_count; current_block++) {
				block_pos    = p;
				block_pos.x += s->raw_blocks[current_block].x;
				block_pos.y += s->raw_blocks[current_block].y;
				
				s->positioned_blocks[current_pos_idx][current_block] = pos_to_offset(block_pos, grid_dim);
			}
		}
	}
	
	s->grid_dim = grid_dim;
	
	// Chain data
	
	s->order       = 0;
	s->blocks_left = 0;
	s->next        = NULL;
	
	// Solve data
	s->correct_position_index = 0;
	
	return s;
}

void shape_destroy(Shape * s) {
	unsigned short i;
	
	for (i = 0; i < s->position_count; i++) free(s->positioned_blocks[i]);
	
	free(s->positioned_blocks);
	free(s->positions);
	free(s->raw_blocks);
	free(s);
}

void shape_dump(Shape * s) {
	unsigned int i;
	char ** repr;
	
	printf("[%02d] Shape (%d x %d, block count %d, blocks left %d, position count %d):\n", (int) s->order, (int) s->dim.w, (int) s->dim.h, (int) s->block_count, (int) s->blocks_left, (int) s->position_count);
		
	repr = malloc(sizeof(char *) * s->dim.h);
	
	for (i = 0; i < s->dim.h; i++) {
		repr[i] = malloc(s->dim.w + 1);
		
		memset(repr[i], ' ', s->dim.w);
		
		repr[i][s->dim.w] = '\0';
	}
	
	for (i = 0; i < s->block_count; i++) {
		repr[s->raw_blocks[i].y][s->raw_blocks[i].x] = '#';
	}
	
	for (i = 0; i < s->dim.h; i++) {
		printf("%s\n", repr[i]);
		free(repr[i]);
	}
	
	free(repr);
}

void shape_fulldump(Shape * s) {
	unsigned i;
	char * repr;
	
	shape_dump(s);
	
	repr = malloc(dim_size(s->grid_dim) + 1);
	
	printf("\nPositions allowed:\n");
	
	for (i = 0; i < s->position_count; i++) {
		printf("\n[%02d] Position (%d, %d)\n", (int) i, (int) s->positions[i].x, (int) s->positions[i].y);
		
		memset(repr, ' ', dim_size(s->grid_dim));
		repr[dim_size(s->grid_dim)] = '\0';
		shape_positiondump(s->positioned_blocks[i], s->block_count, s->grid_dim, repr);

		printf("\n");
	}
	
	free(repr);
}

void shape_positiondump(unsigned short * pblocks, size_t pblock_count, Dimension dim, char * repr) {
	unsigned i;
	char * line;
	
	line = malloc(dim.w + 1);
	
	for (i = 0; i < pblock_count; i++) repr[pblocks[i]] = '#';
	
	for (i = 0; i < dim.h; i++) {
		memcpy(line, repr + (i * dim.w), dim.w);
		line[dim.w] = '\0';
		printf("[%s]\n", line);
	}
	
	free(line);
}


// Shapeset functions


Shape * shapeset_init(char * desc, Dimension grid_dim) {
	char *dcp, *tk, **sds;
	unsigned i;
	size_t sz = 16;
	Shape * first, * cursor;
	
	first  = NULL;
	cursor = NULL;
	
	sds = malloc(sz * sizeof(char *));
	dcp = strdup(desc);
	
	for (i = 0, tk = strtok(dcp, " "); tk; tk = strtok(NULL, " "), i++) {
		if (i >= sz) {
			sz  *= 2;
			sds  = realloc(sds, sz * sizeof(char *));
		}
		
		sds[i] = malloc(strlen(tk) + 1);
		strcpy(sds[i], tk);
	}
	
	free(dcp);
	
	sz  = i;
	sds = realloc(sds, sz * sizeof(char *));
	
	for (i = 0; i < sz; i++) {
		if (i == 0) {
			cursor = shape_init(sds[i], grid_dim);
			first  = cursor;
		}
		else {
			cursor->next = shape_init(sds[i], grid_dim);
			cursor       = cursor->next;
		}
		
		free(sds[i]);
	}
	
	free(sds);
	
	shapeset_ordereddata(first, 1);
	
	return first;
}

void shapeset_ordereddata(Shape * first, unsigned short change_order) {
	Shape * cursor, * cursor2;
	unsigned short i;
	
	for (i = 0, cursor = first; cursor; cursor = cursor->next, i++) {
		if (change_order) cursor->order = i;
		cursor->blocks_left = 0;
		
		for (cursor2 = first; cursor2 != cursor; cursor2 = cursor2->next) {
			cursor2->blocks_left += cursor->block_count;
		}
	}
}

size_t shapeset_sizeof(Shape * first) {
	Shape * s;
	size_t c = 0;
	
	for (s = first; s; s = s->next) c++;
	
	return c;
}

unsigned long long int shapeset_solutioncount(Shape * first) {
	Shape * s;
	unsigned long long int count = 1;
	
	for (s = first; s; s = s->next) {
		count *= (unsigned long long int) s->position_count;
	}
	
	return count;
}

void shapeset_dump(Shape * first) {
	Shape * s;
	
	printf("Shapeset containing %d shapes:\n\n", (int) shapeset_sizeof(first));
	
	for (s = first; s; s = s->next) {
		shape_dump(s);
		printf("\n");
	}
}

void shapeset_destroy(Shape * first) {
	Shape * cursor, * next;
	
	for (cursor = first; cursor; cursor = next) {
		next = cursor->next;
		shape_destroy(cursor);
	}
}

Shape * shapeset_sort(Shape * first, unsigned short int (*compare)(Shape *, Shape *)) {
	first = shapeset__breaksort(first, compare);
	
	shapeset_ordereddata(first, 0);
	
	return first;
}

Shape * shapeset__breaksort(Shape * first, unsigned short int (*compare)(Shape *, Shape *)) {
	Shape * half_one, * half_two, * cursor;

	if ((first == NULL) || (first->next == NULL)) return first;
	
	half_one = first;
	half_two = first;
	cursor   = half_one->next;
	
	while ((cursor != NULL) && (cursor->next != NULL) && (cursor->next->next != NULL)) {
		half_two = half_two->next;
		cursor   = cursor->next->next;
	}
	
	cursor       = half_two;
	half_two     = half_two->next;
	cursor->next = NULL; // Breaking the link
	
	return shapeset__mergesort(shapeset__breaksort(half_one, compare), shapeset__breaksort(half_two, compare), compare);

	
}

Shape * shapeset__mergesort(Shape * half_one, Shape * half_two, unsigned short int (*compare)(Shape *, Shape *)) {
	Shape * first  = NULL;
	Shape * cursor = NULL;
	
	if (half_one == NULL) return half_two;
	if (half_two == NULL) return half_one;
	
	while ((half_one != NULL) && (half_two != NULL)) {
		if (compare(half_one, half_two)) {
			if (first == NULL) {
				first  = half_one;
				cursor = first;
			}
			else {
				cursor->next = half_one;
				cursor       = cursor->next;
			}
			
			half_one = half_one->next;
		}
		else {
			if (first == NULL) {
				first  = half_two;
				cursor = first;
			}
			else {
				cursor->next = half_two;
				cursor       = cursor->next;
			}
			
			half_two = half_two->next;
		}
	}
	
	if (half_one != NULL) cursor->next = half_one;
	if (half_two != NULL) cursor->next = half_two;
	
	return first;
}

unsigned short int shapeset_byorder(Shape * shape_one, Shape * shape_two) {
	unsigned short int one_comes_first = 1;
	
	if (shape_two->order < shape_one->order) one_comes_first = 0;
	
	return one_comes_first;
}

unsigned short int shapeset_byblocks(Shape * shape_one, Shape * shape_two) {
	unsigned short int one_comes_first = 1;
	
	if (shape_two->block_count > shape_one->block_count) one_comes_first = 0;
	
	return one_comes_first;
}



// Grid functions

Grid * grid_init(char * desc) {
	unsigned i, y, ds;
	char * desc_copy, * tk;
	Grid * g;
	
	g        = malloc(sizeof(Grid));
	g->dim.w = 0;
	g->dim.h = 0;
	g->depth = 0;
	
	desc_copy = strdup(desc);
	
	for (tk = strtok(desc_copy, ","); tk; tk = strtok(NULL, ","), g->dim.h++) {
		if (g->dim.w == 0) g->dim.w = strlen(tk);
		
		if (strlen(tk) != g->dim.w) {
			free(desc_copy);
			free(g);
			return NULL;
		}
	}
	
	free(desc_copy);
	
	ds = dim_size(g->dim);
	
	//g->tiles = malloc(sizeof(unsigned short) * ds);
	memset(g->tiles, 0, sizeof(unsigned short) * ds);
	
	for (i = 0; i < ds; i++) {
		y = offset_to_posy(i, g->dim);
		
		g->tiles[i] = desc[i + y] - '0';
		
		if (g->tiles[i] > g->depth) g->depth = g->tiles[i];
	}
	
	g->depth++;
	g->scalar_dim  = ds;
	g->abnormality = grid_abnormality(g);
	
	
	return g;
}

unsigned int grid_abnormality(Grid * g) {
	unsigned int ds, i, abnormality;
	
	ds          = g->scalar_dim;
	abnormality = 0;
	
	for (i = 0; i < ds; i++) {
		if (g->tiles[i] > 0) abnormality += g->depth - g->tiles[i];
	}
	
	return abnormality;
}

void grid_destroy(Grid * g) {
	free(g);
}

void grid_dump(Grid * g) {
	unsigned ds, y, i;
	
	printf("Grid (%d x %d, abnormality %d):\n[", (int) g->dim.w, (int) g->dim.h, (int) g->abnormality);
	
	for (y = 0, i = 0, ds = g->scalar_dim; i < ds; i++) {
		if (y != offset_to_posy(i, g->dim)) {
			y = offset_to_posy(i, g->dim);
			printf("]\n[");
		}
		
		printf("%X", g->tiles[i]);
	}
	
	printf("]\n");
}

// Other functions

unsigned short pos_to_offset(Position pos, Dimension dim) {
	unsigned short offset;
	
	offset = (pos.y * dim.w) + pos.x;
	
	if (offset >= dim_size(dim)) dmabort("Position out of bounds.");
	
	return offset;
}

Position offset_to_pos(unsigned short offset, Dimension dim) {
	Position pos;

	if (offset >= dim_size(dim)) dmabort("Position out of bounds.");
	
	pos.x = offset % dim.w;
	pos.y = (offset - pos.x) / dim.w;
	
	return pos;
}

unsigned short offset_to_posy(unsigned short offset, Dimension dim) {
	if (offset >= dim_size(dim)) dmabort("Position out of bounds.");
	
	return (offset - (offset % dim.w)) / dim.w;
}

unsigned dim_size(Dimension dim) {
	return dim.w * dim.h;
}
