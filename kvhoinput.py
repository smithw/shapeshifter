#!/usr/bin/env python2.5
# -*- encoding: utf-8 -*-

from __future__ import with_statement
import sys

def string_replace(string, idx, char):
	return string[0:idx] + char + string[idx+1:]
	
def make_new_piece(blocks):
	newpiece   = []
	piecewidth = 0
	
	for block in blocks:
		blkx = block % width;
		blky = (block - blkx) / width;
		
		while (len(newpiece) <= blky): newpiece.append("")
		while (len(newpiece[blky]) <= blkx): newpiece[blky] += "."
		
		if blkx > piecewidth: piecewidth = blkx
		
		newpiece[blky] = string_replace(newpiece[blky], blkx, "X")
	

	newpiece = [line.ljust(piecewidth + 1).replace(" ", ".") for line in newpiece]
	
	return ",".join(newpiece)

if __name__ == "__main__":
	fname = sys.argv[1]
	
	pieces = []
	board  = []
	
	with open(fname) as fobj:
		width  = int(fobj.readline()) # first line is width
		height = int(fobj.readline()) # second is height
		
		fobj.readline() # blank line...

		for n in range(height):
			board.append([])
			
			for n in range(width):
				cell = fobj.readline() # read cell
				#print repr(cell)
				board[-1].append(cell.rstrip("\r\n"))
		
		board = ["".join(row) for row in board]
				
		fobj.readline() # blank line...
		fobj.readline() # target
		fobj.readline() # blank line...
		
		numpieces = int(fobj.readline())
		
		
		for i in range(numpieces):
			fobj.readline() # blank line...

			numblocks = int(fobj.readline())
			
			blocks = []

			for j in range(numblocks):
				blocks.append(int(fobj.readline()))
			
			pieces.append(make_new_piece(blocks))
		
	# print board
	print ",".join(board)
	# print pieces
	print " ".join(pieces)
			