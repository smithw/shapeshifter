#include "controller_d.h";

extern unsigned short reverse_search;
extern unsigned skip_nodes;

void * worker_thread(void *);
short worker_iterate(Controller *, SNode *);
void worker_breed(SNode *);
void * manager_thread(void *);
unsigned short manager_iterate(Controller *, SNode *);
void manager_breed(SNode *, unsigned short (*)(SNode *, SNode *));
void * controller_thread(void *);
unsigned short has_it_been_solved(Controller *);
unsigned short generation__compare(SNode *, SNode *);
unsigned short generation__compare_reversed(SNode *, SNode *);
SNode * generation_sort(SNode *, unsigned short (*)(SNode *, SNode *));
SNode * generation__mergesort(SNode *, SNode *, unsigned short (*)(SNode *, SNode *));


// Controller data functions


Controller * controller_init(char * grid_desc, char * ss_desc, size_t worker_count) {
	Controller * con;
	pthread_mutexattr_t recursive;
	extern unsigned short reverse_search;
	
	con       = malloc(sizeof(Controller));
	
	if (!con) return NULL;
	
	con->grid = grid_init(grid_desc);
	
	if (!con->grid) {
		free(con);
		return NULL;
	}
	
	if (pthread_mutexattr_init(&recursive)) {
		grid_destroy(con->grid);
		free(con);
		return NULL;
	}
	
	if (pthread_mutexattr_settype(&recursive, PTHREAD_MUTEX_RECURSIVE)) {
		pthread_mutexattr_destroy(&recursive);
		grid_destroy(con->grid);
		free(con);
		return NULL;
	}
	
	if (
		(pthread_cond_init(&(con->cond_morework), NULL)) || 
		(pthread_cond_init(&(con->cond_workspacefreed), NULL)) || 
		(pthread_cond_init(&(con->cond_finished), NULL)) || 
		(pthread_cond_init(&(con->cond_stats), NULL)) || 
		(pthread_mutex_init(&(con->mx_solved), NULL)) || 
		(pthread_mutex_init(&(con->mx_finished), NULL)) || 
		(pthread_mutex_init(&(con->mx_shape), NULL)) || 
		(pthread_mutex_init(&(con->mx_manager), NULL)) || 
		(pthread_mutex_init(&(con->mx_stats), NULL)) || 
		(pthread_mutex_init(&(con->mx_workqueue), NULL)) ||
		(pthread_mutex_init(&(con->mx_worker_wq), &recursive))
		) {
		
		pthread_mutexattr_destroy(&recursive);
		grid_destroy(con->grid);
		free(con);
		return NULL;
	}

	pthread_mutexattr_destroy(&recursive);
	
	con->first_shape     = shapeset_init(ss_desc, con->grid->dim);
	con->worker_count    = worker_count;
	con->workers         = malloc(sizeof(pthread_t) * worker_count);
	con->solved          = 0;
	con->queue           = NULL;
	con->finished        = 0;
	con->manager_running = 0;
	con->compare         = (reverse_search) ? generation__compare_reversed : generation__compare;
	
	con->stats                 = malloc(sizeof(Stats));
	con->stats->jobs_created   = 0;
	con->stats->jobs_finished  = 0;
	con->stats->jobs_changed   = 0;
	con->stats->jobs_retrieved = 0;
	
	return con;
}

void controller_destroy(Controller * con) {
	grid_destroy(con->grid);
	shapeset_destroy(con->first_shape);
	free(con->workers);
	free(con->stats);
	
	pthread_cond_destroy(&(con->cond_morework));
	pthread_cond_destroy(&(con->cond_workspacefreed));
	pthread_cond_destroy(&(con->cond_finished));
	pthread_cond_destroy(&(con->cond_stats));
	pthread_mutex_destroy(&(con->mx_solved));
	pthread_mutex_destroy(&(con->mx_stats));
	pthread_mutex_destroy(&(con->mx_finished));
	pthread_mutex_destroy(&(con->mx_manager));
	pthread_mutex_destroy(&(con->mx_shape));
	pthread_mutex_destroy(&(con->mx_workqueue));
	pthread_mutex_destroy(&(con->mx_worker_wq));
	
	free(con);
}

void controller_solve(Controller * con) {
	unsigned i;
	
	con->queue = work_queue();
	
	con->first_shape = shapeset_sort(con->first_shape, shapeset_byblocks);
	
	con->manager_running = 1;
	
	pthread_create(&(con->manager), NULL, manager_thread, con);
	pthread_create(&(con->controller), NULL, controller_thread, con);

	for (i = 0; i < con->worker_count; i++) {
		pthread_create(&(con->workers[i]), NULL, worker_thread, con);
	}
}

// Thread functions

void * worker_thread(void * raw_data) {
	Controller * con;
	WQueue * q;
	WNode * work;
	Shape * cursor;
	short solved;
	unsigned short i, stats_broad, manager_running, space_broad;
	unsigned * jobs_changed;
	
	con          = (Controller *) raw_data;
	q            = con->queue;
	solved       = 0;
	jobs_changed = &(con->stats->jobs_changed);
	
	
	do {
		stats_broad     = 0;
		space_broad     = 0;
		manager_running = 1;
		work            = NULL;

		/*pthread_mutex_lock(&(con->mx_workqueue));
		while (!work_get(q, &work)) {
			pthread_cond_wait(&(con->cond_morework), &(con->mx_workqueue));
			
			solved = has_it_been_solved(con);
			if (solved) break;
			
			pthread_mutex_lock(&(con->mx_manager));
			manager_running = con->manager_running;
			pthread_mutex_unlock(&(con->mx_manager));
			
			if (!manager_running) break;
		}
		
		if (WORK_LIMIT - (q->created - q->retrieved) >= WORK_FOR_CONTINUE) space_broad = 1;
		pthread_mutex_unlock(&(con->mx_workqueue));*/
		
		while (work == NULL) {
			work_get(q, &work, 0, &(con->mx_workqueue), &(con->mx_worker_wq), &(con->cond_morework), &(con->cond_workspacefreed));
			
			solved = has_it_been_solved(con);
			if (solved) break;
			
			pthread_mutex_lock(&(con->mx_manager));
			manager_running = con->manager_running;
			pthread_mutex_unlock(&(con->mx_manager));
			
			if (!manager_running) break;
		}
		
		//printf("snode: %p\n", work);
		
		if (solved) break;
		if ((!manager_running) && (work == NULL)) break;
		
		#if WTHREADS > 1
		pthread_mutex_lock(&(con->mx_stats));
		#endif
		con->stats->jobs_retrieved++;
		#if WTHREADS > 1
		pthread_mutex_unlock(&(con->mx_stats));
		#endif

		//if (space_broad) pthread_cond_broadcast(&(con->cond_workspacefreed));
		
		solved = worker_iterate(con, work->snode);
		
		if (solved == 1) {
			pthread_mutex_lock(&(con->mx_shape));
			for (i = 0, cursor = con->first_shape; i < work->snode->generation; i++, cursor = cursor->next)
			cursor->correct_position_index = work->previous_positions[i];
			pthread_mutex_unlock(&(con->mx_shape));
		}
		
		snode_destroy(work->snode);
		work_destroy(work);
		
		#if WTHREADS > 1
		pthread_mutex_lock(&(con->mx_stats));
		con->stats->jobs_finished++;
		#else
		con->stats->jobs_finished++;
		pthread_mutex_lock(&(con->mx_stats));
		#endif
		(*jobs_changed)++;
		
		if ((*jobs_changed) > JOBS_FOR_STATS) stats_broad = 1;
		pthread_mutex_unlock(&(con->mx_stats));
		
		if (stats_broad) pthread_cond_broadcast(&(con->cond_stats));
	}
	while (!solved);
	
	return NULL;
	
}

short worker_iterate(Controller * con, SNode * father) {
	SNode * cursor, * next;
	unsigned short i, tiles_sum;
	short solved;
	unsigned ds;
	
	solved = has_it_been_solved(con);
	
	if (solved) return -1;
	
	if (!father->next_shape) {
		pthread_mutex_lock(&(con->mx_solved));
		if (con->solved) {
			pthread_mutex_unlock(&(con->mx_solved));
			return -1;
		}
		
		ds        = father->grid.scalar_dim;
		tiles_sum = 0;
		
		for (i = 0; i < ds; i++) tiles_sum += father->grid.tiles[i];
		
		if (tiles_sum != 0) {
			printf("In serious trouble we are, young padawan. [%d]\n", (int) tiles_sum);
			grid_dump(&(father->grid));
			pthread_mutex_unlock(&(con->mx_solved));
			return 0;	
		}
		
		con->solved = 1;
		pthread_mutex_unlock(&(con->mx_solved));
		
		pthread_cond_broadcast(&(con->cond_workspacefreed));
		
		return 1;
	}
	
	worker_breed(father);
	
	if (father->children == NULL) return 0;
	
	next = NULL;
	
	for (cursor = father->children; cursor && !solved; cursor = next) {
		next = cursor->next;
		
		solved = worker_iterate(con, cursor);
		
		if (solved == 1) {
			pthread_mutex_lock(&(con->mx_shape));
			father->next_shape->correct_position_index = cursor->last_position_idx;
			pthread_mutex_unlock(&(con->mx_shape));
		}
		
		snode_destroy(cursor);
	}
	
	/*for (cursor = father->children; cursor; cursor = next) {
		next = cursor->next;
		snode_destroy(cursor);
	}*/
	
	return solved;
}

void worker_breed(SNode * father) {
	unsigned pos_idx, blk_idx, depth, generation;
	unsigned short * blocks;
	unsigned short block, grid_tile;
	Shape * s;
	Grid * g, cached;
	SNode * cursor, * prev;
	
	s          = father->next_shape;
	g          = &(father->grid);
	depth      = father->grid.depth;
	generation = father->generation + 1;
	grid_tile  = 0;
	
	cursor = NULL;
	prev   = NULL;
	
	father->children = NULL;
	
	for (pos_idx = 0; pos_idx < s->position_count; pos_idx++) {
		blocks    = s->positioned_blocks[pos_idx];
		memcpy(&cached, g, sizeof(Grid));
		
		for (blk_idx = 0; blk_idx < s->block_count; blk_idx++) {
			block     = blocks[blk_idx];
			grid_tile = cached.tiles[block];

			if (grid_tile == 0) {
				//grid_destroy(cached);
				//cached = NULL;
				break;
			}

			cached.tiles[block] = (grid_tile + 1) % depth;
		}
		
		if (grid_tile == 0) continue;
		
		cursor = snode_create(cached, s->next, father, generation, pos_idx);
		
		if (!father->children) father->children = cursor;
		else prev->next = cursor;
		
		prev = cursor;
	}
}

void * manager_thread(void * raw_data) {
	Controller * con;
	SNode * root;
	Grid grid;
	
	con = (Controller *) raw_data;
	
	memcpy(&grid, con->grid, sizeof(Grid));
	
	root             = snode_create(grid, con->first_shape, NULL, 0, 0);
	root->flips      = ((root->next_shape->block_count + root->next_shape->blocks_left) - root->grid.abnormality) / root->grid.depth;
	
	manager_iterate(con, root);
	
	return NULL;
}

unsigned short manager_iterate(Controller * con, SNode * father) {
	SNode * cursor, * first, * next;
	WNode * work;
	unsigned short solved, i;
	extern unsigned skip_nodes;
	
	solved = has_it_been_solved(con);
	
	if (solved) return 1;
	
	if (father->next_shape == NULL) return 0;
	
	manager_breed(father, con->compare);
	
	if ((father->generation == 0) && (skip_nodes > 0)) {
		for (i = 0, first = father->children; first && i < skip_nodes; i++, first = first->next) ;
	}
	else first = father->children;

	for (cursor = first; cursor; cursor = next) {
		if (father->generation == 0) printf("\nAdvanced on first generation (Pos idx: %d, flips %u, jobs: %llu)\n\n", (int) cursor->last_position_idx, cursor->flips, con->stats->jobs_created);
		
		next = cursor->next;
		
		if (cursor->flips == 0) {
			work = work_create(cursor);
			
			cursor->father = NULL;
			
			work_add(con->queue, work, &(con->mx_workqueue), &(con->cond_workspacefreed));
			
			pthread_cond_signal(&(con->cond_morework));
			
			con->stats->jobs_created++;
		}
		else {
			solved = manager_iterate(con, cursor);
			
			snode_destroy(cursor);
		}
	}
	
	return solved;
}

void manager_breed(SNode * father, unsigned short (*compare)(SNode *, SNode *)) {
	unsigned pos_idx, blk_idx, depth, flip_diff, generation;
	unsigned short * blocks;
	unsigned short block, grid_tile;
	Shape * s;
	Grid * g, cached;
	SNode * cursor, * prev;
	
	s          = father->next_shape;
	g          = &(father->grid);
	depth      = father->grid.depth;
	generation = father->generation + 1;
	
	cursor           = NULL;
	prev             = NULL;
	father->children = NULL;
	
	for (pos_idx = 0; pos_idx < s->position_count; pos_idx++) {
		blocks    = s->positioned_blocks[pos_idx];
		flip_diff = 0;
		memcpy(&cached, g, sizeof(Grid));
		
		for (blk_idx = 0; blk_idx < s->block_count; blk_idx++) {
			block               = blocks[blk_idx];
			grid_tile           = cached.tiles[block];
			cached.tiles[block] = (grid_tile + 1) % depth;

			if (grid_tile == 0) flip_diff++;
		}
		
		if (flip_diff > father->flips) {
			//grid_destroy(cached);
			continue;
		}
		
		cursor                = snode_create(cached, s->next, father, generation, pos_idx);
		cursor->flips         = father->flips - flip_diff;
		
		if (father->children == NULL) father->children = cursor;
		else prev->next = cursor;
		
		prev = cursor;
	}
	
	father->children = generation_sort(father->children, compare);
}

void * controller_thread(void * raw_data) {
	Controller * con;
	unsigned short i;
	
	con = (Controller *) raw_data;
	
	pthread_join(con->manager, NULL);
	
	pthread_mutex_lock(&(con->mx_manager));
	con->manager_running = 0;
	pthread_mutex_unlock(&(con->mx_manager));
	
	printf("Manager finished.\n");
	
/*	pthread_mutex_lock(&(con->mx_workqueue));
	while (con->queue->first) pthread_cond_wait(&(con->cond_workspacefreed), &(con->mx_workqueue));
	pthread_mutex_unlock(&(con->mx_workqueue));
	
	printf("Queue emptied.\n");
	
	solved = has_it_been_solved(con);
	
	if (!solved) {
		pthread_mutex_lock(&(con->mx_solved));
		con->solved = 1;
		pthread_mutex_unlock(&(con->mx_solved));
		
		pthread_cond_broadcast(&(con->cond_morework));
	}*/
	
	pthread_cond_broadcast(&(con->cond_morework));
	
	for (i = 0; i < con->worker_count; i++) pthread_join(con->workers[i], NULL);
	
	work_cleanup(con->queue);
	
	pthread_mutex_lock(&(con->mx_finished));
	con->finished = 1;
	pthread_mutex_unlock(&(con->mx_finished));
	
	con->first_shape = shapeset_sort(con->first_shape, shapeset_byorder);
	
	pthread_mutex_lock(&(con->mx_stats));
	con->stats->jobs_changed = JOBS_FOR_STATS + 1;
	pthread_mutex_unlock(&(con->mx_stats));

	pthread_cond_broadcast(&(con->cond_finished));

	pthread_cond_broadcast(&(con->cond_stats));

	return NULL;
}

unsigned short has_it_been_solved(Controller * con) {
	unsigned short solved;
	
	pthread_mutex_lock(&(con->mx_solved));
	solved = con->solved;
	pthread_mutex_unlock(&(con->mx_solved));
	
	return solved;
}

unsigned short controller_solvingstats(Controller * con, Stats * stats) {
	unsigned short finished;
	
	pthread_mutex_lock(&(con->mx_stats));
	while (con->stats->jobs_changed < JOBS_FOR_STATS) pthread_cond_wait(&(con->cond_stats), &(con->mx_stats));
	con->stats->jobs_changed = 0;
	
	memcpy(stats, con->stats, sizeof(Stats));
	pthread_mutex_unlock(&(con->mx_stats));

	pthread_mutex_lock(&(con->mx_finished));
	finished = con->finished;
	pthread_mutex_unlock(&(con->mx_finished));
	
	return !finished;
}

void controller_stats(Controller * con, Stats * stats) {
	pthread_mutex_lock(&(con->mx_stats));
	memcpy(stats, con->stats, sizeof(Stats));
	pthread_mutex_unlock(&(con->mx_stats));
	
}

unsigned short controller_finished(Controller * con) {
	pthread_mutex_lock(&(con->mx_finished));
	while (!con->finished) pthread_cond_wait(&(con->cond_finished), &(con->mx_finished));
	pthread_mutex_unlock(&(con->mx_finished));
	
	return con->solved;
}


// Search node functions

SNode * snode_create(Grid g, Shape * s, SNode * father, unsigned generation, unsigned last_position_index) {
	SNode * node;

	node                    = malloc(sizeof(SNode));
	
	if (!node) dmabort("Out of memory.");
	
	node->grid              = g;
	node->next_shape        = s;
	node->next              = NULL;
	node->children          = NULL;
	node->father            = father;
	node->generation        = generation;
	node->last_position_idx = last_position_index;
	//printf("+%p\n", node);
	
	return node;
}

void snode_destroy(SNode * node) {
	//printf("-%p\n", node);
	//grid_destroy(node->grid);
	free(node);
}

SNode * generation_sort(SNode * first, unsigned short (*compare)(SNode *, SNode *)) {
	SNode * half_one, * half_two, * cursor;

	if ((first == NULL) || (first->next == NULL)) return first;
	
	half_one = first;
	half_two = first;
	cursor   = half_one->next;
	
	while ((cursor != NULL) && (cursor->next != NULL) && (cursor->next->next != NULL)) {
		half_two = half_two->next;
		cursor   = cursor->next->next;
	}
	
	cursor       = half_two;
	half_two     = half_two->next;
	cursor->next = NULL; // Breaking the link
	
	return generation__mergesort(generation_sort(half_one, compare), generation_sort(half_two, compare), compare);
}

SNode * generation__mergesort(SNode * half_one, SNode * half_two, unsigned short (*compare)(SNode *, SNode *)) {
	SNode * first  = NULL;
	SNode * cursor = NULL;
	
	if (half_one == NULL) return half_two;
	if (half_two == NULL) return half_one;
	
	while ((half_one != NULL) && (half_two != NULL)) {
		if (compare(half_one, half_two)) {
			if (first == NULL) {
				first  = half_one;
				cursor = first;
			}
			else {
				cursor->next = half_one;
				cursor       = cursor->next;
			}
			
			half_one = half_one->next;
		}
		else {
			if (first == NULL) {
				first  = half_two;
				cursor = first;
			}
			else {
				cursor->next = half_two;
				cursor       = cursor->next;
			}
			
			half_two = half_two->next;
		}
	}
	
	if (half_one != NULL) cursor->next = half_one;
	if (half_two != NULL) cursor->next = half_two;
	
	
	return first;
}

unsigned short generation__compare_reversed(SNode * one, SNode * two) {
	unsigned short int one_comes_first = 1;

	if (two->flips >= one->flips) one_comes_first = 0;

	return one_comes_first;
}

unsigned short generation__compare(SNode * one, SNode * two) {
	unsigned short int one_comes_first = 1;
	
	if (two->flips < one->flips) one_comes_first = 0;
	
	return one_comes_first;
}

// Queue functions

WQueue * work_queue(void) {
	WQueue * q;
	
	q            = malloc(sizeof(WQueue));
	if (!q) dmabort("Out of memory.");
	
	q->first     = NULL;
	q->last      = NULL;
	q->created   = 0;
	q->retrieved = 0;
	
	return q;
}

void work_cleanup(WQueue * q) {
	WNode * cur, * next;
	
	printf("Cleaning up...\n");
	
	for (cur = q->first; cur; cur = next) {
		next = cur->next;
		work_destroy(cur);
	}
	
	free(q);
}

WNode * work_create(SNode * snode) {
	WNode * n;
	SNode * cursor;
	unsigned generation;
	int i;
	
	generation            = snode->generation;
	
	n                     = malloc(sizeof(WNode));
	if (!n) dmabort("Out of memory");
	n->snode              = snode;
	n->next               = NULL;
	//n->previous_positions = malloc(sizeof(unsigned) * generation);
	
	for (i = generation - 1, cursor = snode; (i >= 0) && cursor; i--, cursor = cursor->father)
	n->previous_positions[i] = cursor->last_position_idx;
	
	return n;
}

WNode * work_create_simple(void) {
	WNode * n;

	n                     = malloc(sizeof(WNode));
	n->snode              = NULL;
	n->next               = NULL;
	memset(n->previous_positions, 0, MAX_POSITIONS_STORED);
	
	return n;
}

void work_destroy(WNode * n) {
	//free(n->previous_positions);
	free(n);
}

void work_add(WQueue * q, WNode * n, pthread_mutex_t * mx_queue, pthread_cond_t * cond_morespace) {
	if (q->first == NULL) {
		pthread_mutex_lock(mx_queue);
		q->first = n;
		q->last  = n;
		pthread_mutex_unlock(mx_queue);
	}
	else {
		if (q->created - q->retrieved > WORK_LIMIT) {
			pthread_mutex_lock(mx_queue);
			while (q->created - q->retrieved > WORK_LIMIT) {
				//printf("Waiting for work space...[%llu]\n", q->created - q->retrieved);
				pthread_cond_wait(cond_morespace, mx_queue);
				//printf("Work space cleared.\n");
			}
			pthread_mutex_unlock(mx_queue);
			
			work_add(q, n, mx_queue, cond_morespace);
			return;
		}
		
		q->last->next = n;
		q->last       = n;
	}
	
	q->created++;
}

void work_get(WQueue * q, WNode ** n, unsigned short signal_rcvd, pthread_mutex_t * mx_queue, pthread_mutex_t * mx_worker, pthread_cond_t * cond_morework, pthread_cond_t * cond_morespace) {
	unsigned short has_first;
	
	pthread_mutex_lock(mx_worker);

	pthread_mutex_lock(mx_queue);
	has_first = (q->first == NULL) ? 0 : 1;

	if (has_first) {
		
		/*if (q->first->next == NULL) {
			pthread_mutex_lock(mx_manager);
			if (q->first->next == NULL) {
				// We have to make sure the next job is still null, because
				// in the meantime while we lock the mutex it might not be null
				// anymore.
				q->last = NULL;
			}

			*n         = q->first;
			q->first   = q->first->next;
			pthread_mutex_unlock(mx_manager);

			(*n)->next = NULL;
		}
		else {*/

		*n         = q->first;
		q->first   = q->first->next;
		pthread_mutex_unlock(mx_queue);

		q->retrieved++;
		(*n)->next = NULL;
		//}
		
		if (WORK_LIMIT - (q->created - q->retrieved) >= WORK_FOR_CONTINUE) pthread_cond_broadcast(cond_morespace);
	}
	else {
		pthread_mutex_unlock(mx_queue);
		
		// It's a fucking empty queue! How much more space would you need?
		pthread_cond_broadcast(cond_morespace);

		if (signal_rcvd) {
			pthread_mutex_unlock(mx_worker);
			(*n) = NULL;
			return;
		}

		pthread_mutex_lock(mx_queue);
		//printf("Waiting for more work...\n");
		pthread_cond_wait(cond_morework, mx_queue);
		//printf("More work received.\n");
		pthread_mutex_unlock(mx_queue);
		
		work_get(q, n, 1, mx_queue, mx_worker, cond_morework, cond_morespace);
	}
	
	pthread_mutex_unlock(mx_worker);

}
