./shapeshifter 01110,01100,01001,00001,00001,10010 "XXX,..X .XX,XX. .X,XX,X. XXXX .XX,XXX XX.,.XX .X.,XXX,..X,..X ..X.,..X.,XXXX X.,XX,.X .XXX,XX..,.X.. .XX,XX. XXXX,..X.,..X. XX..,.XXX ..X,.XX,XXX .X.,XX.,.XX,.X. XXX,.X. .X,XX,XX"

Final Stats:
Jobs created: 471371
Jobs retrieved: 291485
Jobs finished: 291485

Finished in 2 seconds.


SOLUTION FOUND! ----------------------------------

STEP 01 ------------------------------------------
Put the following piece on position (2, 0) (Position index 2).

[00] Shape (3 x 2, block count 4, blocks left 79, position count 15):
###
  #

The resulting grid will be:

Grid (5 x 6, abnormality 11):
[01001]
[01101]
[01001]
[00001]
[00001]
[10010]
Flips: 34 (34)

STEP 02 ------------------------------------------
Put the following piece on position (2, 4) (Position index 14).

[01] Shape (3 x 2, block count 4, blocks left 75, position count 15):
 ##
## 

The resulting grid will be:

Grid (5 x 6, abnormality 11):
[01001]
[01101]
[01001]
[00001]
[00010]
[10100]
Flips: 32 (32)

STEP 03 ------------------------------------------
Put the following piece on position (3, 2) (Position index 11).

[02] Shape (2 x 3, block count 4, blocks left 71, position count 16):
 #
##
# 

The resulting grid will be:

Grid (5 x 6, abnormality 9):
[01001]
[01101]
[01000]
[00010]
[00000]
[10100]
Flips: 31 (31)

STEP 04 ------------------------------------------
Put the following piece on position (0, 2) (Position index 4).

[03] Shape (4 x 1, block count 4, blocks left 67, position count 12):
####

The resulting grid will be:

Grid (5 x 6, abnormality 11):
[01001]
[01101]
[10110]
[00010]
[00000]
[10100]
Flips: 28 (28)

STEP 05 ------------------------------------------
Put the following piece on position (2, 0) (Position index 2).

[04] Shape (3 x 2, block count 5, blocks left 62, position count 15):
 ##
###

The resulting grid will be:

Grid (5 x 6, abnormality 10):
[01010]
[01010]
[10110]
[00010]
[00000]
[10100]
Flips: 26 (26)

STEP 06 ------------------------------------------
Put the following piece on position (1, 0) (Position index 1).

[05] Shape (3 x 2, block count 4, blocks left 58, position count 15):
## 
 ##

The resulting grid will be:

Grid (5 x 6, abnormality 10):
[00110]
[01100]
[10110]
[00010]
[00000]
[10100]
Flips: 24 (24)

STEP 07 ------------------------------------------
Put the following piece on position (0, 2) (Position index 6).

[06] Shape (3 x 4, block count 6, blocks left 52, position count 9):
 # 
###
  #
  #

The resulting grid will be:

Grid (5 x 6, abnormality 14):
[00110]
[01100]
[11110]
[11110]
[00100]
[10000]
Flips: 19 (19)

STEP 08 ------------------------------------------
Put the following piece on position (0, 0) (Position index 0).

[07] Shape (4 x 3, block count 6, blocks left 46, position count 8):
  # 
  # 
####

The resulting grid will be:

Grid (5 x 6, abnormality 8):
[00010]
[01000]
[00000]
[11110]
[00100]
[10000]
Flips: 19 (19)

STEP 09 ------------------------------------------
Put the following piece on position (0, 0) (Position index 0).

[08] Shape (2 x 3, block count 4, blocks left 42, position count 16):
# 
##
 #

The resulting grid will be:

Grid (5 x 6, abnormality 10):
[10010]
[10000]
[01000]
[11110]
[00100]
[10000]
Flips: 16 (16)

STEP 10 ------------------------------------------
Put the following piece on position (0, 3) (Position index 6).

[09] Shape (4 x 3, block count 6, blocks left 36, position count 8):
 ###
##  
 #  

The resulting grid will be:

Grid (5 x 6, abnormality 10):
[10010]
[10000]
[01000]
[10000]
[11100]
[11000]
Flips: 13 (13)

STEP 11 ------------------------------------------
Put the following piece on position (0, 4) (Position index 12).

[10] Shape (3 x 2, block count 4, blocks left 32, position count 15):
 ##
## 

The resulting grid will be:

Grid (5 x 6, abnormality 6):
[10010]
[10000]
[01000]
[10000]
[10000]
[00000]
Flips: 13 (13)

STEP 12 ------------------------------------------
Put the following piece on position (0, 1) (Position index 2).

[11] Shape (4 x 3, block count 6, blocks left 26, position count 8):
####
  # 
  # 

The resulting grid will be:

Grid (5 x 6, abnormality 10):
[10010]
[01110]
[01100]
[10100]
[10000]
[00000]
Flips: 8 (8)

STEP 13 ------------------------------------------
Put the following piece on position (0, 0) (Position index 0).

[12] Shape (4 x 2, block count 5, blocks left 21, position count 10):
##  
 ###

The resulting grid will be:

Grid (5 x 6, abnormality 7):
[01010]
[00000]
[01100]
[10100]
[10000]
[00000]
Flips: 7 (7)

STEP 14 ------------------------------------------
Put the following piece on position (0, 0) (Position index 0).

[13] Shape (3 x 3, block count 6, blocks left 15, position count 12):
  #
 ##
###

The resulting grid will be:

Grid (5 x 6, abnormality 9):
[01110]
[01100]
[10000]
[10100]
[10000]
[00000]
Flips: 3 (3)

STEP 15 ------------------------------------------
Put the following piece on position (0, 1) (Position index 3).

[14] Shape (3 x 4, block count 6, blocks left 9, position count 9):
 # 
## 
 ##
 # 

The resulting grid will be:

Grid (5 x 6, abnormality 9):
[01110]
[00100]
[01000]
[11000]
[11000]
[00000]
Flips: 0 (0)

STEP 16 ------------------------------------------
Put the following piece on position (1, 0) (Position index 1).

[15] Shape (3 x 2, block count 4, blocks left 5, position count 15):
###
 # 

The resulting grid will be:

Grid (5 x 6, abnormality 5):
[00000]
[00000]
[01000]
[11000]
[11000]
[00000]
Flips: 0 (0)

STEP 17 ------------------------------------------
Put the following piece on position (0, 2) (Position index 8).

[16] Shape (2 x 3, block count 5, blocks left 0, position count 16):
 #
##
##

The resulting grid will be:

Grid (5 x 6, abnormality 0):
[00000]
[00000]
[00000]
[00000]
[00000]
[00000]
Flips: 0 (0)

Solution string: 02000204030200020200010000020000000000030004000100000000000101000002

END OF SOLUTION ----------------------------------
