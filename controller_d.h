#ifndef __CONTROLLER_H__
#define __CONTROLLER_H__

#define WORK_LIMIT 1000000
#define WORK_FOR_CONTINUE 850000
#define JOBS_FOR_STATS 2000000
#define MAX_POSITIONS_STORED 19

#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include "model.h"

typedef struct controller Controller;
typedef struct work_node WNode;
typedef struct work_queue WQueue;
typedef struct search_node SNode;
typedef struct stats Stats;

struct controller {
	pthread_mutex_t mx_solved;
	pthread_mutex_t mx_finished;
	pthread_mutex_t mx_shape;
	pthread_mutex_t mx_workqueue;
	pthread_mutex_t mx_worker_wq;
	pthread_mutex_t mx_stats;
	pthread_mutex_t mx_manager;
	pthread_cond_t cond_stats;
	pthread_cond_t cond_morework;
	pthread_cond_t cond_workspacefreed;
	pthread_cond_t cond_finished;
	unsigned short solved;
	unsigned short finished;
	unsigned short manager_running;
	unsigned short (*compare)(SNode *, SNode *);
	pthread_t * workers;
	pthread_t manager;
	pthread_t controller;
	size_t worker_count;
	Grid * grid;
	Shape * first_shape;
	WQueue * queue;
	Stats * stats;
};


struct stats {
	unsigned long long jobs_created;
	unsigned long long jobs_retrieved;
	unsigned long long jobs_finished;
	unsigned jobs_changed;
};

struct work_node {
	SNode * snode;
	unsigned previous_positions[MAX_POSITIONS_STORED];
	WNode * next;
};

struct work_queue {
	WNode * first;
	WNode * last;
	unsigned long long created;
	unsigned long long retrieved;
};

struct search_node {
	Grid grid;
	Shape * next_shape;
	unsigned flips;
	SNode * next;
	SNode * children;
	SNode * father;
	unsigned last_position_idx;
	unsigned generation;
};

Controller * controller_init(char *, char *, size_t);
void controller_solve(Controller *);
void controller_destroy(Controller *);
unsigned short controller_solvingstats(Controller *, Stats *);
void controller_stats(Controller * con, Stats * stats);
unsigned short controller_finished(Controller *);

SNode * snode_create(Grid, Shape *, SNode *, unsigned, unsigned);
void snode_destroy(SNode *);

WQueue * work_queue(void);
void work_cleanup(WQueue *);
void work_add(WQueue *, WNode *, pthread_mutex_t *, pthread_cond_t *);
void work_get(WQueue *, WNode **, unsigned short, pthread_mutex_t *, pthread_mutex_t *, pthread_cond_t *, pthread_cond_t *);
WNode * work_create(SNode *);
WNode * work_create_simple(void);
void work_destroy(WNode *);

#endif