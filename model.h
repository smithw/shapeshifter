#ifndef __MODEL_H__
#define __MODEL_H__

#define MAX_SCALAR_SIZE 63

typedef struct point Position;
typedef struct shape Shape;
typedef struct grid Grid;
typedef struct dimension Dimension;

#include <stdlib.h>
#include "util.h"

struct point {
	unsigned short x;
	unsigned short y;
};

struct dimension {
	size_t w;
	size_t h;
};

struct shape {
	// Shape data
	Position * raw_blocks;
	size_t block_count;
	Dimension dim;
	
	// Chain data
	unsigned short order;
	size_t blocks_left;
	Shape * next;
	
	// Solve data
	unsigned correct_position_index;

	// Grid-dependend data
	unsigned short ** positioned_blocks;
	Position * positions;
	unsigned position_count;
	Dimension grid_dim;
};

struct grid {
	//unsigned short * tiles;
	unsigned short tiles[MAX_SCALAR_SIZE];
	Dimension dim;
	unsigned scalar_dim;
	unsigned int abnormality;
	unsigned depth;
};


Shape * shape_init(char *, Dimension);
void shape_destroy(Shape *);
void shape_dump(Shape *);
void shape_fulldump(Shape *);

Shape * shapeset_init(char *, Dimension);
size_t shapeset_sizeof(Shape *);
void shapeset_dump(Shape *);
void shapeset_destroy(Shape *);
unsigned long long int shapeset_solutioncount(Shape *);
Shape * shapeset_sort(Shape * first, unsigned short int (*compare)(Shape *, Shape *));
unsigned short int shapeset_byorder(Shape *, Shape *);
unsigned short int shapeset_byblocks(Shape *, Shape *);

Grid * grid_init(char *);
unsigned int grid_abnormality(Grid *);
void grid_destroy(Grid *);
void grid_dump(Grid *);

unsigned short pos_to_offset(Position, Dimension);
Position offset_to_pos(unsigned short, Dimension);
unsigned short offset_to_posy(unsigned short, Dimension);
unsigned dim_size(Dimension);

#endif
