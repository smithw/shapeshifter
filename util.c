#include "util.h"

int readline(char * s, int l) {
	int c, i;

	for (i = 0; (i < l - 1) && ((c = getchar()) != EOF) && (c != '\n'); i++)
		s[i] = c;
	
	s[i++] = '\0';

	return i;
}

unsigned strsplit(char ** dest, char * src, const char * splitter, size_t token_size, size_t token_count) {
	char * src_copy;
	char * token;
	unsigned i;
	
	src_copy = strdup(src);
	
	for (token = strtok(src_copy, splitter), i = 0; token && (i < token_count); token = strtok(NULL, splitter), i++) {
		strncpy(dest[i], token, token_size - 1);
		dest[i][token_size - 1] = '\0';
	}
	
	free(src_copy);
	
	return i;
}

unsigned strsplit_alloc(char *** dest_ptr, char * src, const char * splitter) {
	char * src_copy;
	char * token;
	unsigned i;
	size_t token_count, token_size;
	
	token_count = 16;
	src_copy    = strdup(src);
	(*dest_ptr) = calloc(sizeof(char *), token_count);
	
	for (token = strtok(src_copy, splitter), i = 0; token; token = strtok(NULL, splitter), i++) {
		if (i >= token_count) {
			token_count *= 2;
			(*dest_ptr)  = realloc((*dest_ptr), sizeof(char *) * (token_count));
		}
		
		token_size     = strlen(token) + 1;
		(*dest_ptr)[i] = malloc(token_size);
		
		strcpy((*dest_ptr)[i], token);
	}
	
	free(src_copy);
	
	if (i < token_count) {
		(*dest_ptr) = realloc((*dest_ptr), sizeof(char *) * (i + 1));
	}
	
	return i;
}