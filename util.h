#ifndef UTIL_H
#define UTIL_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define dmabort(MSG) \
 {  printf("Aborting at line %d in source file %s\nError: %s\n",__LINE__,__FILE__,MSG); abort(); }


int readline(char *, int);
unsigned strsplit(char **, char *, const char *, size_t, size_t);
unsigned strsplit_alloc(char ***, char *, const char *);

#endif
