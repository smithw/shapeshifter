#!/bin/bash

GRID=$1
BOARD=$2
LIMIT=$3



SKIPS=`eval LD_LIBRARY_PATH=. ./shapeshifter $GRID \"$BOARD\" maxskip`
SKIPS=`expr $SKIPS - 1`

CMD="LD_LIBRARY_PATH=. ./shapeshifter $GRID \"$BOARD\" autostart giveup $LIMIT skip"

for i in $(seq 0 $SKIPS)
do
	eval $CMD $i
	if [ $? = 0 ]; then
		echo "Solution found; stopping."
		break
	fi
done

