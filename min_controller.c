#include "min_controller.h"

extern unsigned short reverse_search;
extern unsigned skip_nodes;

void * solver_thread(void *);
void * controller_thread(void *);
unsigned short solver_iterate(Controller *, SNode *);
void solver_breed(SNode *, unsigned short (*)(SNode *, SNode *));
void solver_zeroflip_breed(SNode *);
unsigned short generation__compare(SNode *, SNode *);
unsigned short generation__compare_reversed(SNode *, SNode *);
SNode * generation_sort(SNode *, unsigned short (*)(SNode *, SNode *));
SNode * generation__mergesort(SNode *, SNode *, unsigned short (*)(SNode *, SNode *));

Controller * controller_init(char * grid_desc, char * ss_desc) {
	Controller * con;
	extern unsigned short reverse_search;
	
	con       = malloc(sizeof(Controller));
	con->grid = grid_init(grid_desc);
	
	if (!con->grid) {
		free(con);
		return NULL;
	}
	
	if (
		(pthread_cond_init(&(con->cond_stats), NULL)) || 
		(pthread_mutex_init(&(con->mx_stats), NULL))
		) {
		
		grid_destroy(con->grid);
		free(con);
		return NULL;
	}

	con->first_shape = shapeset_init(ss_desc, con->grid->dim);
	con->solved      = 0;
	con->finished    = 0;
	con->compare     = (reverse_search) ? generation__compare_reversed : generation__compare;
	
	con->stats                 = malloc(sizeof(Stats));
	con->stats->jobs_created   = 0;
	con->stats->jobs_finished  = 0;
	con->stats->jobs_changed   = 0;
	con->stats->jobs_retrieved = 0;
	
	return con;
}

void controller_destroy(Controller * con) {
	grid_destroy(con->grid);
	shapeset_destroy(con->first_shape);
	free(con->stats);
	
	pthread_cond_destroy(&(con->cond_stats));
	pthread_mutex_destroy(&(con->mx_stats));
	
	free(con);
}

void controller_solve(Controller * con) {
	con->first_shape = shapeset_sort(con->first_shape, shapeset_byblocks);

	pthread_create(&(con->solver), NULL, solver_thread, con);
	pthread_create(&(con->controller), NULL, controller_thread, con);
}

// Solver functions

void * solver_thread(void * raw_data) {
	Controller * con;
	SNode * root;
	unsigned short solved;
	Grid grid;
	
	con         = (Controller *) raw_data;
	
	memcpy(&grid, con->grid, sizeof(Grid));
	
	root        = snode_create(grid, con->first_shape, 0, 0);
	root->flips = ((root->next_shape->block_count + root->next_shape->blocks_left) - root->grid.abnormality) / root->grid.depth;
	
	solved      = solver_iterate(con, root);
	
	return NULL;
}

void * controller_thread(void * raw_data) {
	Controller * con;
	
	con = (Controller *) raw_data;
	
	pthread_join(con->solver, NULL);
	
	con->first_shape = shapeset_sort(con->first_shape, shapeset_byorder);
		
	pthread_mutex_lock(&(con->mx_stats));
	con->finished = 1;
	con->stats->jobs_changed = JOBS_FOR_STATS + 1;
	pthread_mutex_unlock(&(con->mx_stats));
	
	pthread_cond_broadcast(&(con->cond_stats));
	
	return NULL;
}

unsigned short solver_iterate(Controller * con, SNode * father) {
	SNode * cursor, * next, * first;
	unsigned short i, tiles_sum;
	unsigned short solved, stats_broad;
	unsigned ds;
	extern unsigned skip_nodes;
	
	if (!father->next_shape) {
		if (father->flips == 0) {
			ds        = father->grid.scalar_dim;
			tiles_sum = 0;
		
			for (i = 0; i < ds; i++) tiles_sum += father->grid.tiles[i];
		
			if (tiles_sum != 0) {
				printf("In serious trouble we are, young padawan. [%d - %d]\n", (int) tiles_sum, (int) father->generation);
				grid_dump(&(father->grid));
				return 0;	
			}
			
			con->solved = 1;
		
			return 1;
		}
		else {
			//printf("End node with flips [%d]\n", (int) father->flips);
			return 0;
		}
	}
	
	solved = 0;
	
	if (father->flips == 0) solver_zeroflip_breed(father);
	else solver_breed(father, con->compare);
	
	if ((father->generation == 0) && (skip_nodes > 0)) {
		for (i = 0, first = father->children; first && i < skip_nodes; i++, first = first->next) ;
	}
	else first = father->children;
	
	for (cursor = first; cursor && !solved; cursor = next) {
		if (father->generation == 0) printf("\nAdvanced on first generation (Pos idx: %d, flips %u, jobs: %Lu)\n\n", (int) cursor->last_position_idx, cursor->flips, con->stats->jobs_created);
		
		stats_broad = 0;
		next        = cursor->next;
		
		//breed  = (cursor->flips == 0) ? solver_zeroflip_breed : solver_breed;
		
		if ((cursor->flips == 0) && (father->flips != 0)) {
			con->stats->jobs_created++;

			pthread_mutex_lock(&(con->mx_stats));
			con->stats->jobs_changed++;

			if (con->stats->jobs_changed > JOBS_FOR_STATS) stats_broad = 1;
			pthread_mutex_unlock(&(con->mx_stats));

			if (stats_broad) pthread_cond_broadcast(&(con->cond_stats));
		}
		
		solved = solver_iterate(con, cursor);
		
		if (solved) father->next_shape->correct_position_index = cursor->last_position_idx;

		snode_destroy(cursor);
		
		if (solved) return solved;
	}
	
	return solved;
}

void solver_breed(SNode * father, unsigned short (*compare)(SNode *, SNode *)) {
	unsigned pos_idx, blk_idx, depth, flip_diff, generation;
	unsigned short * blocks;
	unsigned short block, grid_tile;
	Shape * s;
	Grid * g, cached;
	SNode * cursor, * prev;
	
	s          = father->next_shape;
	g          = &(father->grid);
	depth      = father->grid.depth;
	generation = father->generation + 1;
	
	cursor           = NULL;
	prev             = NULL;
	father->children = NULL;
	
	for (pos_idx = 0; pos_idx < s->position_count; pos_idx++) {
		blocks    = s->positioned_blocks[pos_idx];
		flip_diff = 0;
		memcpy(&cached, g, sizeof(Grid));
		
		for (blk_idx = 0; blk_idx < s->block_count; blk_idx++) {
			block               = blocks[blk_idx];
			grid_tile           = cached.tiles[block];
			cached.tiles[block] = (grid_tile + 1) % depth;

			if (grid_tile == 0) flip_diff++;
		}
		
		if (flip_diff > father->flips) {
			continue;
		}
		
		cursor                = snode_create(cached, s->next, generation, pos_idx);
		cursor->flips         = father->flips - flip_diff;
		
		if (father->children == NULL) father->children = cursor;
		else prev->next = cursor;
		
		prev = cursor;
	}
	
	father->children = generation_sort(father->children, compare);
}

void solver_zeroflip_breed(SNode * father) {
	unsigned pos_idx, blk_idx, depth, generation;
	unsigned short * blocks;
	unsigned short block, grid_tile;
	Shape * s;
	Grid * g, cached;
	SNode * cursor, * prev;
	
	s          = father->next_shape;
	g          = &(father->grid);
	depth      = father->grid.depth;
	generation = father->generation + 1;
	grid_tile  = 0;
	
	cursor = NULL;
	prev   = NULL;
	
	father->children = NULL;
	
	for (pos_idx = 0; pos_idx < s->position_count; pos_idx++) {
		blocks    = s->positioned_blocks[pos_idx];
		//cached    = grid_copy(g);
		memcpy(&cached, g, sizeof(Grid));
		
		for (blk_idx = 0; blk_idx < s->block_count; blk_idx++) {
			block                = blocks[blk_idx];
			grid_tile            = cached.tiles[block];

			if (grid_tile == 0) {
				//grid_destroy(cached);
				//cached = NULL;
				break;
			}

			cached.tiles[block] = (grid_tile + 1) % depth;
		}
		
		if (grid_tile == 0) continue;
		
		cursor = snode_create(cached, s->next, generation, pos_idx);
		cursor->flips = 0;
		
		if (!father->children) father->children = cursor;
		else prev->next = cursor;
		
		prev = cursor;
	}
}

unsigned short controller_solvingstats(Controller * con, Stats * stats) {
	unsigned short finished;
	
	pthread_mutex_lock(&(con->mx_stats));
	while (con->stats->jobs_changed < JOBS_FOR_STATS) {
		//printf("waiting for stats.\n");
		pthread_cond_wait(&(con->cond_stats), &(con->mx_stats));
	}
	con->stats->jobs_changed = 0;
	
	memcpy(stats, con->stats, sizeof(Stats));

	finished = con->finished;
	pthread_mutex_unlock(&(con->mx_stats));

	return !finished;
}

void controller_stats(Controller * con, Stats * stats) {
	pthread_mutex_lock(&(con->mx_stats));
	memcpy(stats, con->stats, sizeof(Stats));
	pthread_mutex_unlock(&(con->mx_stats));
	
}


// Search node functions

SNode * snode_create(Grid g, Shape * s, unsigned generation, unsigned last_position_index) {
	SNode * node;

	node                    = malloc(sizeof(SNode));
	node->grid              = g;
	node->next_shape        = s;
	node->next              = NULL;
	node->children          = NULL;
	node->generation        = generation;
	node->last_position_idx = last_position_index;
	//printf("+%p\n", node);
	
	return node;
}

void snode_destroy(SNode * node) {
	//printf("-%p\n", node);
	//grid_destroy(node->grid);
	free(node);
}

SNode * generation_sort(SNode * first, unsigned short (*compare)(SNode *, SNode *)) {
	SNode * half_one, * half_two, * cursor;

	if ((first == NULL) || (first->next == NULL)) return first;
	
	half_one = first;
	half_two = first;
	cursor   = half_one->next;
	
	while ((cursor != NULL) && (cursor->next != NULL) && (cursor->next->next != NULL)) {
		half_two = half_two->next;
		cursor   = cursor->next->next;
	}
	
	cursor       = half_two;
	half_two     = half_two->next;
	cursor->next = NULL; // Breaking the link
	
	return generation__mergesort(generation_sort(half_one, compare), generation_sort(half_two, compare), compare);
}

SNode * generation__mergesort(SNode * half_one, SNode * half_two, unsigned short (*compare)(SNode *, SNode *)) {
	SNode * first  = NULL;
	SNode * cursor = NULL;
	
	if (half_one == NULL) return half_two;
	if (half_two == NULL) return half_one;
	
	while ((half_one != NULL) && (half_two != NULL)) {
		if (compare(half_one, half_two)) {
			if (first == NULL) {
				first  = half_one;
				cursor = first;
			}
			else {
				cursor->next = half_one;
				cursor       = cursor->next;
			}
			
			half_one = half_one->next;
		}
		else {
			if (first == NULL) {
				first  = half_two;
				cursor = first;
			}
			else {
				cursor->next = half_two;
				cursor       = cursor->next;
			}
			
			half_two = half_two->next;
		}
	}
	
	if (half_one != NULL) cursor->next = half_one;
	if (half_two != NULL) cursor->next = half_two;
	
	
	return first;
}

unsigned short generation__compare_reversed(SNode * one, SNode * two) {
	unsigned short int one_comes_first = 1;
	extern unsigned short reverse_search;
	
	if (two->flips >= one->flips) one_comes_first = 0;
	
	return one_comes_first;
}

unsigned short generation__compare(SNode * one, SNode * two) {
	unsigned short int one_comes_first = 1;
	extern unsigned short reverse_search;
	
	if (two->flips < one->flips) one_comes_first = 0;
	
	return one_comes_first;
}