#ifndef __CONTROLLER_H__
#define __CONTROLLER_H__

#define JOBS_FOR_STATS 500000

#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include "model.h"

typedef struct controller Controller;
typedef struct search_node SNode;
typedef struct stats Stats;

struct controller {
	pthread_t solver;
	pthread_t controller;
	pthread_mutex_t mx_stats;
	pthread_cond_t cond_stats;
	unsigned short solved;
	unsigned short finished;
	unsigned short (*compare)(SNode *, SNode *);
	Grid * grid;
	Shape * first_shape;
	Stats * stats;
};

struct stats {
	unsigned long long jobs_created;
	unsigned long long jobs_retrieved;
	unsigned long long jobs_finished;
	unsigned jobs_changed;
};


struct search_node {
	Grid grid;
	Shape * next_shape;
	unsigned flips;
	SNode * next;
	SNode * children;
	unsigned last_position_idx;
	unsigned generation;
};

Controller * controller_init(char *, char *);
void controller_solve(Controller *);
void controller_destroy(Controller *);
unsigned short controller_solvingstats(Controller *, Stats *);
void controller_stats(Controller * con, Stats * stats);
unsigned short controller_finished(Controller *);

SNode * snode_create(Grid, Shape *, unsigned, unsigned);
void snode_destroy(SNode *);


#endif