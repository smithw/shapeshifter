#ifndef __CONTROLLER_H__
#define __CONTROLLER_H__

#define WORK_LIMIT 3000000
#define WORK_FOR_CONTINUE 39850000
#define JOBS_FOR_STATS 3000000
#define MAX_POSITIONS_STORED 20

#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include "model.h"

typedef struct controller Controller;
typedef struct work_node WNode;
typedef struct work_queue WQueue;
typedef struct search_node SNode;
typedef struct stats Stats;

struct controller {
	pthread_mutex_t mx_solved;
	pthread_mutex_t mx_finished;
	pthread_mutex_t mx_shape;
	pthread_mutex_t mx_stats;
	pthread_mutex_t mx_manager;
	pthread_cond_t cond_stats;
	pthread_cond_t cond_finished;
	unsigned short should_cancel;
	unsigned short solved;
	unsigned short finished;
	unsigned short manager_running;
	unsigned short (*compare)(SNode *, SNode *);
	pthread_t * workers;
	pthread_t manager;
	pthread_t controller;
	size_t worker_count;
	Grid * grid;
	Shape * first_shape;
	WQueue * queue;
	Stats * stats;
};


struct stats {
	unsigned long long jobs_created;
	unsigned long long jobs_retrieved;
	unsigned jobs_changed;
};

struct work_node {
	SNode * snode;
	unsigned previous_positions[MAX_POSITIONS_STORED];
};

struct work_queue {
	WNode buffer[WORK_LIMIT];
	WNode * tail;
	WNode * head;
};

struct search_node {
	Grid grid;
	Shape * next_shape;
	unsigned flips;
	SNode * next;
	SNode * children;
	SNode * father;
	unsigned last_position_idx;
	unsigned generation;
	unsigned previous_positions[MAX_POSITIONS_STORED];
};

Controller * controller_init(char *, char *, size_t);
void controller_solve(Controller *);
void controller_destroy(Controller *);
unsigned short controller_solvingstats(Controller *, Stats *);
void controller_stats(Controller * con, Stats * stats);
unsigned short controller_finished(Controller *);
void controller_set_properties(unsigned short, unsigned);

SNode * snode_create(Grid, Shape *, SNode *, unsigned, unsigned);
void snode_destroy(SNode *);

WQueue * work_queue();
WNode work_create(SNode * snode);
unsigned short work_enqueue(WQueue *, WNode);
unsigned short work_dequeue(WQueue *, WNode *);



#endif
