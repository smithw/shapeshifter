UNAME := $(shell uname)

CC=gcc
CFLAGS=-O3 -I. -W -Wall
THREADFLAGS=-lpthread
LIBFLAGS=-fPIC

ifeq ($(UNAME), Darwin)
DYLIB=-dynamiclib
DYLIBEXT=.dylib
DYN=-dynamic
endif

ifeq ($(UNAME), Linux)
DYLIB=-shared
DYLIBEXT=.so
DYN=
endif

shapeshifter: libshapeshifter${DYLIBEXT} main.c
	${CC} ${CFLAGS} ${DYN} -L. -o shapeshifter main.c -lshapeshifter

shapeshifter_static: main.c util.o model.o controller.o
	${CC} ${CFLAGS} -o shapeshifter_static main.c util.o model.o controller.o ${THREADFLAGS}

libshapeshifter.dylib: util_lib.o model_lib.o controller_lib.o
	${CC} ${CFLAGS} ${LIBFLAGS} ${DYLIB} -o libshapeshifter.dylib util_lib.o model_lib.o controller_lib.o ${THREADFLAGS}

libshapeshifter.so: util_lib.o model_lib.o controller_lib.o
	${CC} ${CFLAGS} ${LIBFLAGS} ${DYLIB}  -Wl,-soname,libshapeshifter.so,-export-dynamic -o libshapeshifter.so util_lib.o model_lib.o controller_lib.o ${THREADFLAGS}

dyn: main.c util.o model.o controller_d.o
	${CC} ${CFLAGS} -o shapeshifter_d main.c util.o model.o controller_d.o ${THREADFLAGS}

controller_d.o: controller_d.h controller_d.c
	${CC} ${CFLAGS} -c controller_d.c -o controller_d.o

util.o: util.h util.c
	${CC} ${CFLAGS} -c util.c -o util.o

model.o: model.h model.c
	${CC} ${CFLAGS} -c model.c -o model.o
	
controller.o: controller.h controller.c
	${CC} ${CFLAGS} -c controller.c -o controller.o

util_lib.o: util.h util.c
	${CC} ${CFLAGS} ${LIBFLAGS} -c util.c -o util_lib.o

model_lib.o: model.h model.c
	${CC} ${CFLAGS} ${LIBFLAGS} -c model.c -o model_lib.o
	
controller_lib.o: controller.h controller.c
	${CC} ${CFLAGS} ${LIBFLAGS} -c controller.c -o controller_lib.o
	
statqueue.o: statqueue.h statqueue.c
	${CC} ${CFLAGS} -DTHREADED_QUEUE -c statqueue.c -o statqueue.o
	
clean_static:
	rm -f util.o model.o controller.o shapeshifter_static

clean:
	rm -f util_lib.o model_lib.o controller_lib.o libshapeshifter${DYLIBEXT} shapeshifter

