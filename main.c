#include <stdlib.h>
#include <time.h>
#include "model.h"
#include "controller.h"

#define WTHREADS 2

unsigned short reverse_search;
unsigned skip_nodes;

void solution_dump(Grid *, Shape *);

int main(int argc, char ** argv) {
	Controller * con;
	time_t start, finish;
	long double elapsed;
	char * goon;
	int goon_read;
	Stats stats;
	unsigned short i, autostart, maxskip;
	unsigned long long int solution_count;
	unsigned int threads;
	unsigned long long giveup;
	
	if (argc < 3) return EXIT_FAILURE;

	threads        = WTHREADS;
	reverse_search = 0;
	skip_nodes     = 0;
	giveup         = 0L;
	autostart      = 0;
	maxskip        = 0;
	
	if (argc > 3) {
		for (i = 3; i < argc; i++) {
			if (!strcmp(argv[i], "reverse")) {
				reverse_search = 1;
			}
			if (!strcmp(argv[i], "autostart")) {
				autostart = 1;
			}
			if (!strcmp(argv[i], "maxskip")) {
				maxskip = 1;
			}
			else if (!strcmp(argv[i], "skip")) {
				i++;
				if (i >= argc) break;
				if (!sscanf(argv[i], "%u", &skip_nodes)) {
					skip_nodes = 0;
					i--;
					continue;
				}
			}
			else if (!strcmp(argv[i], "threads")) {
				i++;
				if (i >= argc) break;
				if (!sscanf(argv[i], "%u", &threads)) {
					threads = WTHREADS;
					i--;
					continue;
				}
			}
			else if (!strcmp(argv[i], "giveup")) {
				i++;
				if (i >= argc) break;
				if (!sscanf(argv[i], "%llu", &giveup)) {
					giveup = 0L;
					i--;
					continue;
				}
			}
		}
	}

	controller_set_properties(reverse_search, skip_nodes);
	
	con = controller_init(argv[1], argv[2], threads);
	
	if (!con) return EXIT_FAILURE;

	if (maxskip) {
		con->first_shape = shapeset_sort(con->first_shape, shapeset_byblocks);
		printf("%u\n", con->first_shape->position_count);
		return EXIT_SUCCESS;
	}
	
	solution_count = shapeset_solutioncount(con->first_shape);

	printf("Shapeshifter Solver 4.0\n(c) 2009-10, Ugo Pozo\n\n");
	
	printf("These are the values you entered:\n\n");
	
	grid_dump(con->grid);
	printf("Flips: %lu\n\n", (con->first_shape->block_count + con->first_shape->blocks_left - con->grid->abnormality) / con->grid->depth);
	shapeset_dump(con->first_shape);
	
	printf("\nDepth: %u\n", con->grid->depth);
	printf("Number of possible solutions: %llu\n", solution_count);
	printf("Number of worker threads to use: %u\n", threads);	
	printf("Reverse search: %s\n", (reverse_search) ? "Yes" : "No");
	printf("Skip first generation nodes: %u\n\n", skip_nodes);
	

	printf("Is everything correct? [Y/n] ");
	
	if (!autostart) {
		goon = malloc(5);
		goon_read = readline(goon, 4);
		
		if ((!goon_read) || (!strcmp(goon, "n")) || (!strcmp(goon, "N"))) {
			free(goon);
			controller_destroy(con);
			return EXIT_SUCCESS;
		}
		
		free(goon);
	}
	else printf("(autostart)");
	
	printf("\n");

	start = time(NULL);
	controller_solve(con);
	
	while (controller_solvingstats(con, &stats)) {
		elapsed = (long double) time(NULL) - (long double) start;
		printf("Jobs created: %llu\n", stats.jobs_created);
		printf("Jobs retrieved: %llu\n", stats.jobs_retrieved);
		printf("Time elapsed: %Lgs\n", elapsed);

		if (giveup && (stats.jobs_created > giveup)) {
			printf("\nJob limit (%llu) achieved. Canceling...\n", giveup);
			con->should_cancel = 1;
		}

		printf("\n");
	}

	controller_stats(con, &stats);
	printf("\nFinal Stats:\nJobs created: %llu\n", stats.jobs_created);
	printf("Jobs retrieved: %llu\n", stats.jobs_retrieved);
//	printf("Jobs finished: %llu\n\n", stats.jobs_finished);

	finish = time(NULL);
	elapsed = (long double) finish - (long double) start;
	
	printf("Finished in %Lg seconds.\n", elapsed);

	if (con->solved)
	solution_dump(con->grid, con->first_shape);
	else printf("\nOoops. No solution was found.\n");

	printf("\nFinished in %Lg seconds.\n", elapsed);
	printf("From skip %u, at %llu/%llu created/retrieved jobs.\n", skip_nodes, stats.jobs_created, stats.jobs_retrieved);
	
	
	controller_destroy(con);
	
	return (con->solved) ? EXIT_SUCCESS : EXIT_FAILURE;
}

void solution_dump(Grid *gg, Shape * first) {
	Grid g;
	int tokens, flips;
	Shape * s;//, * first;
	char * solution_string;
	unsigned int i = 1;
	unsigned j, pos_idx;
	
	//g      = grid_copy(gg);
	memcpy(&g, gg, sizeof(Grid));
	tokens = g.depth;
	flips  = (first->blocks_left + first->block_count - g.abnormality) / tokens;
	
	solution_string    = malloc(4 * shapeset_sizeof(first) + 1);
	solution_string[0] = '\0';
	
	printf("\n\nSOLUTION FOUND! ----------------------------------\n");

	for (s = first; s; s = s->next, i++) {
		pos_idx = s->correct_position_index;

		printf("\nSTEP %02d ------------------------------------------", (int) i);
		printf("\nPut the following piece on position (%d, %d) (Position index %d).\n\n", s->positions[pos_idx].x, s->positions[pos_idx].y, (int) pos_idx);
		shape_dump(s);
		printf("\nThe resulting grid will be:\n\n");
		
		for (j = 0; j < s->block_count; j++) {
			if (g.tiles[s->positioned_blocks[pos_idx][j]] == 0) flips--;
			g.tiles[s->positioned_blocks[pos_idx][j]] = (g.tiles[s->positioned_blocks[pos_idx][j]] + 1) % tokens;
		}
		g.abnormality = grid_abnormality(&g);
		
		grid_dump(&g);
		printf("Flips: %ld (%d)\n", (s->blocks_left - g.abnormality) / tokens, flips);
		sprintf(solution_string, "%s%02d%02d", solution_string, s->positions[pos_idx].x, s->positions[pos_idx].y);
	}

	printf("\nSolution string: %s\n\nEND OF SOLUTION ----------------------------------\n", solution_string);
	
	free(solution_string);
}
